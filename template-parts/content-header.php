<header class="entry-header">
	<div class="container">
        <div class="row">
            <div class="col-lg-8">
                <?php
                if ( is_singular() ) :
                    the_title( '<h1 class="entry-title">', '</h1>' );
                else :
                    the_title( '<h1 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h1>' );
                endif; ?>
            </div>
            <div class="col-lg-4">
                <small>
                <?php
                    if ( function_exists('yoast_breadcrumb') ) {
                        yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
                    }
                ?>
                </small>
            </div>
        </div>
	</div>
</header><!-- .entry-header -->