<?php hsf_2020_post_thumbnail(); ?>
<div class="post-header">
<?php
			if ( is_singular() ) :
				the_title( '<h1 class="entry-title">', '</h1>' );
			else :
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			endif;

			if ( 'post' === get_post_type() ) :
				?>
				<div class="entry-meta"> <small>
					<?php
					hsf_2020_posted_on();
					?>
				</small></div><!-- .entry-meta -->
			<?php endif; ?>
</div>