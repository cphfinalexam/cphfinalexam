<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package HSF_2020
 */

get_header();
?>
<div class="container">
	<div id="primary" class="content-area">
		<main id="main" class="site-main">

			<section class="entry-header">
				<div class="container">
					<div class="row">
						<header class="page-header">
							<h1 class="page-title"><?php esc_html_e( 'HOV! Den side findes ikke.', 'hsf_2020' ); ?></h1>
						</header><!-- .page-header -->
					</div>
				</div>
				<div class="row post-site">
					<div class="page-content col-lg-8">
						<p><?php esc_html_e( 'Du har fundet ind på en side som ikke eksisterer. Søgte du efter noget?', 'hsf_2020' ); ?></p>
						<?php get_search_form();?>
					</div><!-- .page-content -->
					<div class="sidebar col-lg-4">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
</div>
<?php
get_footer();