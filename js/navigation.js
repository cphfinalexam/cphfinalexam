/**
 * File navigation.js.
 *
 * Handles toggling the navigation menu for small screens and enables TAB key
 * navigation support for dropdown menus.
 */

// Function for the search bar in Top-menu
var topMenu = document.getElementById('top-menu');
var searchForm = topMenu.getElementsByClassName('bop-nav-search')[0]; //It's a HTMLCollection, therefore [0] to get the <li> element
var searchField = searchForm.firstElementChild[0];

if(window.addEventListener){
	searchForm.addEventListener('submit', checkOnSubmit, true);
}

function checkOnSubmit(e){
	if(getComputedStyle(searchField).display === "none"){ //Check if searchField exist
		searchField.style.display = 'inline-block';
		e.preventDefault();
	} else if (searchField.value === "" && searchField.style.display === 'inline-block'){
		searchField.style.display = 'none';
		e.preventDefault();
	} else{
		return true;
	}
};

// NAVIGATION TOGGLER
var siteNav = document.getElementById('site-navigation');
var topNav = document.getElementById('top-navigation');
var topMenu = topNav.querySelector('#top-menu');
var toggleButton = document.getElementById('navbar-toggler');
var i = 0;

function toggle(){
	if (i === 0){
		i++;
		toggleButton.classList.add("is-active");
		siteNav.style.display = "flex";
		topNav.style.display = "flex";
		topNav.style.padding = "10px";
		topNav.style.marginTop = "25px";
		topMenu.style.display = "flex";
	}else{
		i--;
		toggleButton.classList.remove("is-active");
		siteNav.style.display = "none";
		topNav.style.display = "none";
		topNav.style.padding = "0";
		topNav.style.marginTop = "0px";
		topMenu.style.display = "none";
	}
};

//Adds '   |' after menu Items in Primary menu if screen > 767px
var primaryMenu = document.getElementById('primary-menu').getElementsByClassName('menu-item');
var primaryMenuArrayLenght = primaryMenu.length;

console.log("menuItemArray = " + primaryMenuArrayLenght);

for (var j = 0; j < primaryMenuArrayLenght; j++){
	if (primaryMenu[j].getElementsByTagName('a')[0].innerHTML === "Bliv medlem"){
		primaryMenu[j].classList.add("redBtn");
	}

	if (screen.width > 767){
		console.log(primaryMenu[j].getElementsByTagName('a')[0].innerHTML);
		if (primaryMenu[j].getElementsByTagName('a')[0].innerHTML === "Bliv medlem"){
			
		} else{
			primaryMenu[j].getElementsByTagName('a')[0].innerHTML += "   |";
		}
	};
};







/** 
 * Function from Underscore we don´t use
 * (It was a button-toggler for the navigation,
 *  but didn't work then we had two menus)
 * - Sara
 * 
 */  

// ( function() {
// 	var container, button, menu, links, i, len;

// 	container = document.getElementById( 'site-navigation' );
// 	if ( ! container ) {
// 		return;
// 	}
	
// 	button = container.getElementsByTagName( 'button' )[0];
// 	if ( 'undefined' === typeof button ) {
// 		return;
// 	}

// 	menu = container.getElementsByTagName( 'ul' )[0];

// 	// Hide menu toggle button if menu is empty and return early.
// 	if ( 'undefined' === typeof menu ) {
// 		button.style.display = 'none';
// 		return;
// 	}

// 	if ( -1 === menu.className.indexOf( 'nav-menu' ) ) {
// 		menu.className += ' nav-menu';
// 	}

// 	button.onclick = function() {
// 		if ( -1 !== container.className.indexOf( 'toggled' ) ) {
// 			container.className = container.className.replace( ' toggled', '' );
// 			button.setAttribute( 'aria-expanded', 'false' );
// 		} else {
// 			container.className += ' toggled';
// 			button.setAttribute( 'aria-expanded', 'true' );
// 		}
// 	};

// 	// Close small menu when user clicks outside
// 	document.addEventListener( 'click', function( event ) {
// 		var isClickInside = container.contains( event.target );

// 		if ( ! isClickInside ) {
// 			container.className = container.className.replace( ' toggled', '' );
// 			button.setAttribute( 'aria-expanded', 'false' );
// 		}
// 	} );

// 	// Get all the link elements within the menu.
// 	links = menu.getElementsByTagName( 'a' );

// 	// Each time a menu link is focused or blurred, toggle focus.
// 	for ( i = 0, len = links.length; i < len; i++ ) {
// 		links[i].addEventListener( 'focus', toggleFocus, true );
// 		links[i].addEventListener( 'blur', toggleFocus, true );
// 	}

// 	/**
// 	 * Sets or removes .focus class on an element.
// 	 */
// 	function toggleFocus() {
// 		var self = this;

// 		// Move up through the ancestors of the current link until we hit .nav-menu.
// 		while ( -1 === self.className.indexOf( 'nav-menu' ) ) {
// 			// On li elements toggle the class .focus.
// 			if ( 'li' === self.tagName.toLowerCase() ) {
// 				if ( -1 !== self.className.indexOf( 'focus' ) ) {
// 					self.className = self.className.replace( ' focus', '' );
// 				} else {
// 					self.className += ' focus';
// 				}
// 			}

// 			self = self.parentElement;
// 		}
// 	}

// 	/**
// 	 * Toggles `focus` class to allow submenu access on tablets.
// 	 */
// 	( function() {
// 		var touchStartFn,
// 			parentLink = container.querySelectorAll( '.menu-item-has-children > a, .page_item_has_children > a' );

// 		if ( 'ontouchstart' in window ) {
// 			touchStartFn = function( e ) {
// 				var menuItem = this.parentNode;

// 				if ( ! menuItem.classList.contains( 'focus' ) ) {
// 					e.preventDefault();
// 					for ( i = 0; i < menuItem.parentNode.children.length; ++i ) {
// 						if ( menuItem === menuItem.parentNode.children[i] ) {
// 							continue;
// 						}
// 						menuItem.parentNode.children[i].classList.remove( 'focus' );
// 					}
// 					menuItem.classList.add( 'focus' );
// 				} else {
// 					menuItem.classList.remove( 'focus' );
// 				}
// 			};

// 			for ( i = 0; i < parentLink.length; ++i ) {
// 				parentLink[i].addEventListener( 'touchstart', touchStartFn, false );
// 			}
// 		}
// 	}( container ) );
// }() );