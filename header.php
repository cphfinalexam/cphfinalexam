<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package HSF_2020
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'hsf_2020' ); ?></a>

	<header id="masthead" class="site-header navbar navbar-expand-lg navbar-light bg-light">
		<div class="container">
			<div class="row">
				<div class="site-branding navbar-brand">
					<?php the_custom_logo();?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<!-- Site description removed from here /SS -->
				</div><!-- .site-branding -->

				<button id="navbar-toggler" type="button" onclick="toggle()">
					<span class="line" id="test"></span>
          			<span class="line"></span>
          			<span class="line"></span>
				</button> <!-- Navbar toogler -->

				<nav id="top-navigation" class="main-navigation navbar-nav">
					<!-- <button class="menu-toggle" aria-controls="top-menu" aria-expanded="false"><?php// esc_html_e( 'top Menu', 'hsf_2020' ); ?></button> -->
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-2',
							'menu_id' 		 => 'top-menu',
							'link_after'	 => '  |',
							)
					);
					?>
				</nav><!-- #top-navigation -->
				<nav id="site-navigation" class="main-navigation navbar-nav">
					<!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php// esc_html_e( 'Primary Menu', 'hsf_2020' ); ?></button> -->
					<?php
					wp_nav_menu(
						array(
							'theme_location' => 'menu-1',
							'menu_id'        => 'primary-menu',
						)
					);
					?>
				</nav><!-- #site-navigation -->
			</div> <!-- Row end -->
				
				
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
