<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package HSF_2020
 */

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php if ( have_posts() ) : ?>

			<header class="entry-header">
				<div class="container">
        			<div class="row">
						<div class="col-lg-8">
							<?php
							the_archive_title( '<h1 class="entry-title">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
							?>
						</div>
						<div class="col-lg-4">
							<small>
								<?php
									if ( function_exists('yoast_breadcrumb') ) {
										yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );
									}
								?>
							</small>
						</div>
					</div>
				</div>
			</header><!-- .entry-header -->
			<div class="container">
				<div class="row post-site">
					<div class="col-lg-8">
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post(); ?>
						<div class="searchResult">
							<?php the_title( sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

							<!-- /*
							* Include the Post-Type-specific template for the content.
							* If you want to override this in a child theme, then include a file
							* called content-___.php (where ___ is the Post Type name) and that will be used instead.
							*/ -->
							<div class="entry-meta">
								<?php
								hsf_2020_posted_on();
								?>
							</div><!-- .entry-meta -->
							<div class="entry-summary">
								<?php the_excerpt(); ?>
							</div><!-- .entry-summary -->
							<footer class="entry-footer">
								<?php echo '<a href="', esc_url( get_permalink()) ,'"><button class="elementor-button-link elementor-button elementor-size-sm readMoreButton">Læs mere</button></a>'; ?>
							</footer><!-- .entry-footer -->
						</div>
						
					<?php endwhile;

					the_posts_navigation();

					else :

						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>
					</div>
					<div class="sidebar col-lg-4">
						<?php get_sidebar(); ?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
